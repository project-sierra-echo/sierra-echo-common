package echo.common.domain.permanent;

import echo.core.domain.model.EventSource;
import jakarta.persistence.Column;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class AbstractExistence<O extends EventSource> {

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "deleted_by", updatable = false)
  private O deletedBy;

  @Column(name = "deletion_timestamp", updatable = false)
  private LocalDateTime deletedTimestamp;

  public O getDeletedBy() {
    return deletedBy;
  }

  public void setDeletedBy(O deletedBy) {
    this.deletedBy = deletedBy;
  }

  public LocalDateTime getDeletedTimestamp() {
    return deletedTimestamp;
  }

  public void setDeletedTimestamp(LocalDateTime deletedTimestamp) {
    this.deletedTimestamp = deletedTimestamp;
  }
}
