package echo.common.domain.permanent;

import echo.common.domain.audit.jpa.OperationRole;
import jakarta.persistence.Embeddable;

@Embeddable
public class Existence extends AbstractExistence<OperationRole> {

}
