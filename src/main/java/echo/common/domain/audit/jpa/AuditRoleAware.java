package echo.common.domain.audit.jpa;

import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.lang.NonNull;

public class AuditRoleAware implements AuditorAware<OperationRole> {

  private static final OperationRole SERVICE_ROLE;

  static {
    SERVICE_ROLE = new OperationRole();

    SERVICE_ROLE.setId(0L);
  }

  @NonNull
  @Override
  public Optional<OperationRole> getCurrentAuditor() {
    return Optional.of(SERVICE_ROLE);
  }
}
