package echo.common.domain.audit.jpa;

import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EntityScan("echo.common.domain.audit.jpa")
@EnableJpaAuditing(dateTimeProviderRef = "auditingDateTimeProvider")
public class JpaAuditingConfiguration {

  @Bean
  public AuditRoleAware operationRoleAudit() {
    return new AuditRoleAware();
  }

  @Bean
  public DateTimeProvider auditingDateTimeProvider() {
      return () -> Optional.of(LocalDateTime.now());
  }
}
