package echo.common.domain.audit.jpa;

import echo.core.domain.model.EventSource;
import jakarta.persistence.Column;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@MappedSuperclass
public abstract class AbstractAudit<O extends EventSource> {

  @CreatedBy
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "created_by", nullable = false, updatable = false)
  private O creator;

  @LastModifiedBy
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "updated_by", nullable = false)
  private O updater;

  @CreatedDate
  @Column(name = "creation_timestamp", nullable = false, updatable = false)
  private LocalDateTime createdTimestamp;

  @LastModifiedDate
  @Column(name = "update_timestamp", nullable = false)
  private LocalDateTime updatedTimestamp;

  protected AbstractAudit() {

  }

  protected AbstractAudit(O creator, O updater, LocalDateTime createdTimestamp,
      LocalDateTime updatedTimestamp) {
    this.creator = creator;
    this.updater = updater;
    this.createdTimestamp = createdTimestamp;
    this.updatedTimestamp = updatedTimestamp;
  }

  public O getCreator() {
    return creator;
  }

  public void setCreator(O creator) {
    this.creator = creator;
  }

  public O getUpdater() {
    return updater;
  }

  public void setUpdater(O updater) {
    this.updater = updater;
  }

  public LocalDateTime getCreatedTimestamp() {
    return createdTimestamp;
  }

  public void setCreatedTimestamp(LocalDateTime createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
  }

  public LocalDateTime getUpdatedTimestamp() {
    return updatedTimestamp;
  }

  public void setUpdatedTimestamp(LocalDateTime updatedTimestamp) {
    this.updatedTimestamp = updatedTimestamp;
  }
}
