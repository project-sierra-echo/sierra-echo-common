package echo.common.domain.audit.jpa;

import jakarta.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Audit extends AbstractAudit<OperationRole>
  implements Serializable {

}
